import * as React from 'react'
import { Image, ImageProps, View } from 'react-native'
import * as FileSystem from 'expo-file-system'

type Override<T, U> = Omit<T, keyof U> & U

type CachedImageProps = Override<ImageProps, { source: string | undefined }>

const CachedImage: React.FC<CachedImageProps> = ({
  source = '',
  ...props
}: CachedImageProps) => {
  const filesystemURI = React.useMemo(() => {
    const lastSlashIndex = source.lastIndexOf('/')
    if (lastSlashIndex === -1) return ''
    const cacheKey = source.slice(lastSlashIndex + 1)
    return `${FileSystem.cacheDirectory}${cacheKey}`
  }, [source])

  const [imgURI, setImgURI] = React.useState<string | null>(filesystemURI)

  const componentIsMounted = React.useRef(true)

  React.useEffect(() => {
    const loadImage = async (fileURI: string) => {
      try {
        // Use the cached image if it exists
        const metadata = await FileSystem.getInfoAsync(fileURI)
        if (!metadata.exists) {
          // download to cache
          if (componentIsMounted.current) {
            setImgURI(null)
            await FileSystem.downloadAsync(source, fileURI)
          }
          if (componentIsMounted.current) {
            setImgURI(fileURI)
          }
        }
      } catch (err) {
        console.log(err)
        setImgURI(source)
      }
    }

    if (filesystemURI) loadImage(filesystemURI)

    return () => {
      componentIsMounted.current = false
    }
  }, [filesystemURI])
  if (!imgURI) return <View {...props} />
  return <Image {...props} source={{ uri: imgURI }} />
}

export default CachedImage
